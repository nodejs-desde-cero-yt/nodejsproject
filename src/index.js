// Importación de Datos de otros ficheros ES6
import { object1 } from './objects.js';
import { getAuthor, getTitle } from './functions.js';

// Importación de Datos Legacy
// const { object1 } = require('./objects');
// const { getAuthor, getTitle } = require('./functions');

console.log('Inicializando Proyecto NodeJs...');

console.log(getTitle());
console.log(getAuthor('Jean Carlo Arevalo Diaz'));
console.log(object1);

// Variables de Ambiente
console.log(process.env.PORT || 80);