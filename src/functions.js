// Exportación de Datos ES6
export const getTitle = () => 'Curso de NodeJs desde Cero.';

export const getAuthor = (author) => `El autor del proyecto es: ${author}`;

//Exportación de Datos Legacy
// module.exports = {
//     getTitle,
//     getAuthor
// };